from string import punctuation
import re
from epg import *


class NeighboursVrtData:
    """
    Class for fetching and aggregating data about Neighbours
    from the different APIs. Data is collected on a best effort basis.
    """

    def __init__(self, date=None):
        """
        :param date: Can be a datetime instance, a YYYYMMDD string,
        a YYYY-MM-DD string, a UTC timestamp, or a delta in days with today.
        Default is today.
        """
        self.vrt_epg = VrtEPG('een', date)
        self.een_epg = EenWebsiteEPG(date)

    def __bool__(self):
        return self.vrt_epg.contains('Buren')

    def _clean_text(self, text):
        """
        Remove html tags, html symbols and surrounding whitespace from the given text.
        Based on https://stackoverflow.com/a/12982689
        """
        return re.sub('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});', '', text).strip()

    def _clean_url(self, url):
        if url.startswith('//'):
            return 'http:' + url
        elif url.startswith('/'):
            return 'https://images.vrt.be' + url
        return url

    @property
    def vrt_epg_data(self):
        return self.vrt_epg.get_program('Buren')

    @property
    def een_epg_data(self):
        return self.een_epg.get_program('Buren')

    @property
    def datetime(self):
        return self.vrt_epg.get_datetime('Buren')

    @property
    def time(self):
        return self.vrt_epg.get_start_time('Buren')

    @property
    def year(self):
        return self.vrt_epg.date.strftime('%Y')

    @property
    def episode(self):
        try:
            return self._clean_text(self.een_epg_data['episodeNumber'])
        except TypeError:
            return None

    @property
    def subtitle(self):
        try:
            return self._clean_text(self.vrt_epg_data['subtitle'])
        except TypeError:
            return None

    @property
    def subtitle_with_ellipsis(self):
        if self.subtitle[-1:] not in punctuation:
            return self.subtitle + '…'
        else:
            return self.subtitle

    @property
    def summary(self):
        try:
            return self._clean_text(self.een_epg_data['description'])
        except TypeError:
            return None

    @property
    def image_url(self):
        try:
            return self._clean_url(self._clean_text(self.vrt_epg_data['image']))
        except TypeError:
            return None

    @property
    def vrt_nu_url(self):
        try:
            if self.vrt_epg_data['url'] is not None:
                return 'https://www.vrt.be' + self._clean_text(self.vrt_epg_data['url'])
        except (KeyError, TypeError):
            pass
        try:
            if self.een_epg_data['vrtnu'] is not None:
                return self._clean_text(self.een_epg_data['vrtnu'])
        except (KeyError, TypeError):
            pass
        return 'https://www.vrt.be/vrtnu/a-z/buren/{year}/buren-s{year}a{episode}/'.format(year=self.year, episode=self.episode)

    @property
    def on_demand_url(self):
        return self.vrt_nu_url


class NeighboursVtmData:
    """
    Class for fetching and aggregating data about Neighbours
    from the different APIs. Data is collected on a best effort basis.
    """
    setting_channel = 'VTM 2'
    setting_title = 'Neighbours'

    def __init__(self, date=None):
        """
        :param date: Can be a datetime instance, a YYYYMMDD string,
        a YYYY-MM-DD string, a UTC timestamp, or a delta in days with today.
        Default is today.
        """
        self.epg = VtmEPG(self.setting_channel, date)
        self._detail_epg = None

    @property
    def detail_epg(self):
        if self._detail_epg:
            return self._detail_epg
        else:
            if self.episode_id:
                self._detail_epg = VtmDetailEPG(self.episode_id, self.setting_channel, self.epg.date)
                return self._detail_epg
            else:
                return None

    def __bool__(self):
        return self.epg.contains(self.setting_title)

    def _first_sentence(self, text):
        try:
            match = re.search('(\.|\?|!)', text)
            if match.group() == '.':
                return text[:match.start()]
            else:
                return text[:match.start()+1]
        except (ValueError, AttributeError):
            return text

    def _clean_text(self, text):
        """
        Remove html tags, html symbols and surrounding whitespace from the given text.
        Based on https://stackoverflow.com/a/12982689
        """
        return re.sub('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});', '', text).strip()

    def _clean_url(self, url):
        if url.startswith('//'):
            return 'http:' + url
        elif url.startswith('/'):
            return 'https://images.vrt.be' + url  # TODO: Change to VTM
        return url

    @property
    def epg_data(self):
        return self.epg.get_program(self.setting_title, offset=-1)

    @property
    def detail_epg_data(self):
        if self.detail_epg:
            return self.detail_epg.get_program(self.setting_title, offset=-1)

    @property
    def datetime(self):
        return self.epg.get_datetime(self.setting_title, offset=-1)

    @property
    def time(self):
        return self.epg.get_start_time(self.setting_title, offset=-1)

    @property
    def year(self):
        return self.epg.date.strftime('%Y')

    @property
    def episode_id(self):
        try:
            return self._clean_text(self.epg_data['uuid'])
        except TypeError:
            return None

    @property
    def on_demand_id(self):
        try:
            return 'e' + self._clean_text(self.epg_data['playableUuid'])
        except TypeError:
            return None

    @property
    def episode(self):
        if self.detail_epg:
            try:
                return self._clean_text(str(self.detail_epg_data['order']))
            except TypeError:
                return None

    @property
    def subtitle(self):
        try:
            return self._first_sentence(self._clean_text(self.epg_data['synopsis']))
        except TypeError:
            return None

    @property
    def subtitle_with_ellipsis(self):
        if self.subtitle[-1:] not in punctuation:
            return self.subtitle + '…'
        else:
            return self.subtitle

    @property
    def summary(self):
        if self.detail_epg:
            try:
                return self._clean_text(self.detail_epg_data['synopsisOriginal'])
            except TypeError:
                return None

    @property
    def image_url(self):
        if self.detail_epg:
            try:
                return self._clean_url(self._clean_text(self.detail_epg_data['imageUrl']))
            except TypeError:
                return None

    @property
    def on_demand_url(self):
        return 'https://vtm.be/vtmgo/afspelen/{playableUuid}'.format(playableUuid=self.on_demand_id)


class NeighboursData(NeighboursVtmData):

    pass


class NeighboursPlayData:
    """
    Class for fetching and aggregating data about Neighbours
    from the different APIs. Data is collected on a best effort basis.
    """
    setting_channel = 'Play 5'
    setting_title = 'Neighbours: A New Chapter'

    def __init__(self, date=None, offset=None):
        """
        :param date: Can be a datetime instance, a YYYYMMDD string,
        a YYYY-MM-DD string, a UTC timestamp, or a delta in days with today.
        Default is today.
        """
        self.epg = PlayEPG(self.setting_channel, date)
        if offset:
            self.offset = offset
        else:
            self.offset = self._determine_afternoon_offset()
        self._detail_epg = None

    @property
    def detail_epg(self):
        if self._detail_epg:
            return self._detail_epg
        else:
            if self.season and self.episode_new:
                self._detail_epg = PlayDetailEPG(
                    'neighbours-a-new-chapter',
                    self.season,
                    self.episode_new
                )
                return self._detail_epg
            else:
                return None

    def __bool__(self):
        return self.epg.contains(self.setting_title)

    def _determine_afternoon_offset(self):
        offset = 0
        if self.epg.contains(self.setting_title):
            start_time: datetime = self.epg.get_datetime(self.setting_title, offset)
            if start_time is not None:
                while start_time is not None and start_time.hour < 12:
                    # If the program starts in the morning, try to find an offset where it starts in the afternoon
                    # TODO: This code is not optimal, but it works for now
                    # offset += 1
                    start_time = self.epg.get_datetime(self.setting_title, offset)

                    if start_time is None or start_time.hour >= 23 or start_time.hour <= 3:
                        # If there is no program at this offset, go back one
                        # or if the program starts in the middle of the night, go back one
                        offset -= 1
                        break
                    elif start_time.hour >= 12:  # If the program starts in the afternoon, use this offset
                        break
                    else:
                        offset += 1  # Else, try the next offset
        return offset

    def _first_sentence(self, text):
        try:
            match = re.search('(\.|\?|!)', text)
            if match.group() == '.':
                return text[:match.start()]
            else:
                return text[:match.start()+1]
        except (ValueError, AttributeError):
            return text

    def _clean_text(self, text):
        """
        Remove html tags, html symbols and surrounding whitespace from the given text.
        Based on https://stackoverflow.com/a/12982689
        """
        return re.sub('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});', '', text).strip()

    def _clean_url(self, url):
        if url.startswith('//'):
            return 'http:' + url
        elif url.startswith('/'):
            return 'https://wmimages.goplay.be' + url
        else:
            return url

    @property
    def epg_data(self):
        return self.epg.get_program(self.setting_title, offset=self.offset)

    @property
    def detail_epg_data(self):
        if self.detail_epg:
            return self.detail_epg.get_data()

    @property
    def datetime(self):
        return self.epg.get_datetime(self.setting_title, offset=self.offset)

    @property
    def time(self):
        return self.epg.get_start_time(self.setting_title, offset=self.offset)

    @property
    def year(self):
        return self.epg.date.strftime('%Y')

    @property
    def season(self):
        try:
            return self._clean_text(self.epg_data['season'])
        except TypeError:
            return None

    @property
    def episode_new(self):
        try:
            return self._clean_text(self.epg_data['episodeNr'])
        except TypeError:
            return None

    @property
    def episode(self):
        if self.episode_new is not None:
            return str(int(self.episode_new) + 8903)

    @property
    def subtitle(self):
        try:
            return self._first_sentence(self._clean_text(self.epg_data['contentEpisode']))
        except TypeError:
            return None

    @property
    def subtitle_with_ellipsis(self):
        if self.subtitle[-1:] not in punctuation:
            return self.subtitle + '…'
        else:
            return self.subtitle

    @property
    def summary(self):
        try:
            return self._clean_text(self.epg_data['video_node']['description'])
        except TypeError:
            return None

    @property
    def image_url(self):
        try:
            if self.detail_epg:
                return self._clean_url(self._clean_text(self.detail_epg.get_image_url()))
        except TypeError:
            return None

    @property
    def on_demand_url(self):
        # The following code does not work in case epg_data['video_node']['latest_video'] is False
        # try:
        #     relative_url = self._clean_text(self.epg_data['video_node']['url'])
        #     return 'https://www.goplay.be' + relative_url
        # except TypeError:
        #     return None
        return self.detail_epg.get_url()
