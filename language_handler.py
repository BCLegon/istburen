import random
from string import punctuation, whitespace


class Spice():

    @classmethod
    def random_choice_from(cls, lst, chance=1.00):
        if chance == 1 or random.random() < chance:
            return random.choice(lst)
        else:
            return ''

    @classmethod
    def random_choice_from_hierarchy(cls, lst):
        if isinstance(lst, (list, tuple, set)):
            return cls.random_choice_from_hierarchy(cls.random_choice_from(lst))
        else:
            return lst


class SpiceNL(Spice):

    @classmethod
    def only(cls):
        return cls.random_choice_from(['enkel', 'enkel', 'alleen', 'alleen maar'])

    @classmethod
    def no(cls):
        return 'geen'
        # return cls.random_choice_from(['geen','geen enkele keer'])

    @classmethod
    def excluding(cls):
        return cls.random_choice_from(['behalve', 'uitgezonderd'])

    @classmethod
    def unfortunately(cls, chance=1.00):
        return cls.random_choice_from(['helaas', 'helaas', 'jammer genoeg', 'spijtig genoeg'], chance)


class TextGeneratorNL():
    spice = SpiceNL

    def __init__(self):
        self.words = []

    def __str__(self):
        def check(word):
            return word[:1] not in '!,.:;?' and word[:1] not in whitespace
        return ''.join(' ' + word if check(word) else word for word in self.words)[1:]

    def _add_word(self, word, index=None):
        if word:
            if index != None:
                self.words.insert(index, word)
            else:
                self.words.append(word)

    def _add_words(self, words, index=None):
        if index:
            for i in range(len(words) - 1, -1, -1):
                self._add_word(words[i], index)
        else:
            for word in words:
                self._add_word(word)
