import datetime, requests, json, re
from bs4 import BeautifulSoup


class VrtEPG:
    """
    Class for fetching information from the VRT API,
    as used in the VRT NU program guide.
    Contains data for the channels Een, Canvas and Ketnet.
    """
    channel = None
    channel_code = None
    date = None
    epg_data = None
    base_url = 'https://www.vrt.be/bin/epg/schedule.{date}.json'

    def __init__(self, channel, date=None):
        self.set_channel(channel)
        self.set_date(date)

    def set_channel(self, channel):
        if channel == 1 or channel.lower() == 'een' or channel.lower() == 'één':
            self.channel = 'een'
            self.channel_code = 'O8'
        elif channel == 2 or channel.lower() == 'canvas':
            self.channel = 'canvas'
            self.channel_code = '1H'
        elif channel == 3 or channel == 12 or channel.lower() == 'ketnet':
            self.channel = 'ketnet'
            self.channel_code = 'O9'

    def set_date(self, date=None):
        """
        :param date: Can be a datetime instance, a YYYYMMDD string,
        a YYYY-MM-DD string, a UTC timestamp, or a delta in days with today.
        Default is today.
        """
        if isinstance(date, datetime.datetime):
            self.date = date
        elif isinstance(date, str):
            try:
                self.date = datetime.datetime.strptime(date[:8], '%Y%m%d')
            except ValueError:
                try:
                    self.date = datetime.datetime.strptime(date[:10], '%Y-%m-%d')
                except ValueError:
                    raise ValueError('Incorrect date format, should be YYYYMMDD or YYYY-MM-DD')
        elif isinstance(date, int):
            if date > 1e8:
                self.date = datetime.datetime.utcfromtimestamp(date)
            else:
                self.date = datetime.datetime.now() + datetime.timedelta(days=date)
        else:
            self.date = datetime.datetime.now()

    def get_date_string(self):
        return self.date.strftime('%Y-%m-%d')

    def get_api_url(self):
        return self.base_url.format(date=self.get_date_string())

    def get_epg_data(self):
        if self.epg_data:
            return self.epg_data
        else:
            request = requests.get(self.get_api_url())
            self.epg_data = json.loads(request.content)[self.channel_code]
            return self.epg_data

    def print_epg_data(self):
        print(json.dumps(self.get_epg_data(), indent=4))

    def get_program(self, title, offset=0):
        title = title.lower()
        epg_data = self.get_epg_data()
        airings = [elem for elem in epg_data if elem['title'].lower() == title]
        if len(airings):
            return airings[offset]
        return None

    def contains(self, title):
        elem = self.get_program(title)
        if elem:
            return True
        else:
            return False

    def _from_iso_format(self, date_time):
        try:
            return datetime.datetime.fromisoformat(date_time)
        except AttributeError:
            utcoffset_pos = date_time.find('+')
            # Remove colons for backwards compatibility with %z directive
            while date_time[utcoffset_pos:].find(':') >= 0:
                colon_pos = utcoffset_pos + date_time[utcoffset_pos:].find(':')
                date_time = date_time[:colon_pos] + date_time[colon_pos + 1:]
            return datetime.datetime.strptime(date_time, '%Y-%m-%dT%H:%M:%S%z')

    def get_datetime(self, title, offset=0):
        elem = self.get_program(title, offset)
        if elem:
            return self._from_iso_format(elem['startTime'])

    def get_start_time(self, title, offset=0):
        elem = self.get_program(title, offset)
        if elem:
            time = self._from_iso_format(elem['startTime'])
            return time.strftime('%H:%M')

    def get_end_time(self, title, offset=0):
        elem = self.get_program(title, offset)
        if elem:
            time = self._from_iso_format(elem['endTime'])
            return time.strftime('%H:%M')


class EenWebsiteEPG(VrtEPG):
    """
    Class for fetching information from the custom Een API,
    as used on the een.be website.
    Only contains data for the channel Een.
    """
    base_url = 'https://www.een.be/api/epg/event/'

    def __init__(self, date=None):
        super().__init__('een', date)

    def get_date_string(self):
        return self.date.strftime('%Y%m%d')

    def get_api_url(self):
        return self.base_url + self.get_date_string()

    def get_epg_data(self):
        if self.epg_data:
            return self.epg_data
        else:
            request = requests.get(self.get_api_url())
            self.epg_data = json.loads(request.content)['data']
            return self.epg_data

    def get_start_time(self, title, offset=0):
        elem = self.get_program(title, offset)
        if elem:
            time = datetime.datetime.utcfromtimestamp(elem['startTimeUnix'])\
                .replace(tzinfo=datetime.timezone.utc).astimezone(tz=None)
            return time.strftime('%H:%M')

    def get_end_time(self, title, offset=0):
        elem = self.get_program(title, offset)
        if elem:
            time = datetime.datetime.utcfromtimestamp(elem['endTimeUnix'])\
                .replace(tzinfo=datetime.timezone.utc).astimezone(tz=None)
            return time.strftime('%H:%M')

    def get_episode(self, title, offset=0):
        elem = self.get_program(title, offset)
        if elem:
            return elem['episodeNumber']

    def get_description(self, title, offset=0):
        elem = self.get_program(title, offset)
        if elem:
            return elem['description']

    def get_url(self, title=None, offset=None):
        if title:
            elem = self.get_program(title, offset)
            if elem:
                return elem['epgUrl']
        else:
            return 'https://www.een.be/tv-gids#?date=' + self.get_date_string()


class VtmEPG:
    """
    Class for fetching information from the VTM API,
    as used in the VTM program guide.
    Contains data for the channels VTM, VTM 2, VTM 3, VTM 4, VTM Gold, VTM Kids and Qmusic.
    """
    channel = None
    date = None
    epg_data = None
    base_url = 'https://vtm.be/tv-gids/dag/{date}'

    def __init__(self, channel='VTM', date=None):
        self.set_channel(channel)
        self.set_date(date)

    def set_channel(self, channel):
        if isinstance(channel, int):
            if channel == 1:
                self.channel = 'vtm'
            else:
                self.channel = 'vtm {digit}'.format(digit=channel)
        elif channel.lower() == 'gold':
            self.channel = 'vtm gold'
        elif channel.lower() == 'kids':
            self.channel = 'vtm kids'
        elif channel.lower() == 'q':
            self.channel = 'qmusic'
        else:
            self.channel = channel.lower()

    def set_date(self, date=None):
        """
        :param date: Can be a datetime instance, a YYYYMMDD string,
        a YYYY-MM-DD string, a UTC timestamp, or a delta in days with today.
        Default is today.
        """
        if isinstance(date, datetime.datetime):
            self.date = date
        elif isinstance(date, str):
            try:
                self.date = datetime.datetime.strptime(date[:8], '%Y%m%d')
            except ValueError:
                try:
                    self.date = datetime.datetime.strptime(date[:10], '%Y-%m-%d')
                except ValueError:
                    raise ValueError('Incorrect date format, should be YYYYMMDD or YYYY-MM-DD')
        elif isinstance(date, int):
            if date > 1e8:
                self.date = datetime.datetime.utcfromtimestamp(date)
            else:
                self.date = datetime.datetime.now() + datetime.timedelta(days=date)
        else:
            self.date = datetime.datetime.now()

    def get_date_string(self):
        return self.date.strftime('%d-%m-%Y')

    def get_api_url(self):
        return self.base_url.format(date=self.get_date_string())

    def get_epg_data(self):
        if self.epg_data:
            return self.epg_data
        else:
            url = self._bypass_cookies(self.get_api_url())
            # Get the page segment that contains the data in js code
            request = requests.get(url)
            soup = BeautifulSoup(request.text, 'html.parser')
            try:
                script_data = soup.find(id='__EPG_DATA__').contents[0]
                # Extract valid json data from the js code
                query = '__EPG_REDUX_DATA__='
                start = script_data.find(query) + len(query)
                end = start + script_data[start:].find(';\n')
                data = json.loads(script_data[start:end])
                # Filter by the correct channel, or extract details
                self.epg_data = self._extract_and_filter(data['details'], data['broadcasts'], data['channels'])
            except AttributeError:
                self.epg_data = {}
            return self.epg_data

    def _bypass_cookies(self, url):
        request = requests.get(url)
        query = 'https://vtm.be/privacygate-confirm?'
        start = request.text.find(query)
        end = start + request.text[start:].find('\'')
        url = request.text[start:end]
        return url

    def _extract_and_filter(self, details, broadcasts, channels):
        # Filter by channel
        channel_broadcasts = []
        for channel_data in channels.values():
            if channel_data['name'].lower() == self.channel or channel_data['seoKey'].lower() == self.channel:
                channel_broadcasts = channel_data['broadcasts']
                break
        broadcasts = {key: broadcasts[key] for key in channel_broadcasts}
        return broadcasts

    def print_epg_data(self):
        print(json.dumps(self.get_epg_data(), indent=4))

    def get_program(self, title, offset=0):
        title = title.lower()
        epg_data = self.get_epg_data()
        airings = [elem for elem in epg_data.values() if elem['title'].lower() == title]
        if len(airings):
            return airings[offset]
        return None

    def contains(self, title):
        elem = self.get_program(title)
        if elem:
            return True
        else:
            return False

    def _from_iso_format(self, date_time):
        try:
            return datetime.datetime.fromisoformat(date_time)
        except AttributeError:
            utcoffset_pos = date_time.find('+')
            # Remove colons for backwards compatibility with %z directive
            while date_time[utcoffset_pos:].find(':') >= 0:
                colon_pos = utcoffset_pos + date_time[utcoffset_pos:].find(':')
                date_time = date_time[:colon_pos] + date_time[colon_pos + 1:]
            return datetime.datetime.strptime(date_time, '%Y-%m-%dT%H:%M:%S%z')

    def _get_utc_timezone(self):
        return datetime.timezone(datetime.timedelta(0))

    def _get_local_timezone(self):
        now = datetime.datetime.now()
        local_tz = datetime.timezone(
            datetime.datetime.fromtimestamp(now.timestamp()) -
            datetime.datetime.utcfromtimestamp(now.timestamp())
        )
        return local_tz

    def get_datetime(self, title, offset=0):
        elem = self.get_program(title, offset)
        if elem:
            return self._from_iso_format(elem['fromIso'])

    def get_start_time(self, title, offset=0):
        elem = self.get_program(title, offset)
        if elem:
            time = self._from_iso_format(elem['fromIso'])
            time = time.replace(tzinfo=self._get_utc_timezone()).astimezone(self._get_local_timezone())
            return time.strftime('%H:%M')

    def get_end_time(self, title, offset=0):
        elem = self.get_program(title, offset)
        if elem:
            time = self._from_iso_format(elem['toIso'])
            return time.strftime('%H:%M')


class VtmDetailEPG(VtmEPG):
    episode_id = None
    base_url = 'https://vtm.be/tv-gids/vtm/uitzending/aflevering/{id}'

    def __init__(self, episode_id, channel='VTM', date=None):
        self.episode_id = episode_id
        super(VtmDetailEPG, self).__init__(channel, date)

    def get_api_url(self):
        return self.base_url.format(id=self.episode_id)

    def _extract_and_filter(self, details, broadcasts, channels):
        # Extract episode details
        return details[self.episode_id]

    def get_program(self, title=None, offset=None):
        if title and not self.contains(title):
            return None
        return self.get_epg_data()

    def get_title(self):
        return self.get_epg_data()['title']

    def contains(self, title):
        if self.get_title().lower() == title.lower():
            return True
        else:
            return False


class PlayEPGv1:
    """
    Class for fetching information from the Play API,
    as used in the Play program guide.
    Contains data for the channels Play4, Play5, Play6, Play7 and Play Crime.
    """
    channel = None
    date = None
    epg_data = None
    base_url = 'https://www.goplay.be/api/epg/{channel}/{date}'

    def __init__(self, channel, date=None):
        self.set_channel(channel)
        self.set_date(date)

    def set_channel(self, channel):
        if channel == 4 or channel.lower().replace(" ", "") == 'play4' or channel.lower() == 'vier':
            self.channel = 'vier'
        elif channel == 5 or channel.lower().replace(" ", "") == 'play5' or channel.lower() == 'vijf':
            self.channel = 'vijf'
        elif channel == 6 or channel.lower().replace(" ", "") == 'play6' or channel.lower() == 'zes':
            self.channel = 'zes'
        elif channel == 7 or channel.lower().replace(" ", "") == 'play7' or channel.lower() == 'zeven':
            self.channel = 'zeven'
        elif channel.lower().replace(" ", "") == 'playcrime' or channel.lower() == 'crime':
            self.channel = 'crime'

    def set_date(self, date=None):
        """
        :param date: Can be a datetime instance, a YYYYMMDD string,
        a YYYY-MM-DD string, a UTC timestamp, or a delta in days with today.
        Default is today.
        """
        if isinstance(date, datetime.datetime):
            self.date = date
        elif isinstance(date, str):
            try:
                self.date = datetime.datetime.strptime(date[:8], '%Y%m%d')
            except ValueError:
                try:
                    self.date = datetime.datetime.strptime(date[:10], '%Y-%m-%d')
                except ValueError:
                    raise ValueError('Incorrect date format, should be YYYYMMDD or YYYY-MM-DD')
        elif isinstance(date, int):
            if date > 1e8:
                self.date = datetime.datetime.utcfromtimestamp(date)
            else:
                self.date = datetime.datetime.now() + datetime.timedelta(days=date)
        else:
            self.date = datetime.datetime.now()

    def get_date_string(self):
        return self.date.strftime('%Y-%m-%d')

    def get_api_url(self):
        return self.base_url.format(channel=self.channel, date=self.get_date_string())

    def get_epg_data(self):
        if self.epg_data:
            return self.epg_data
        else:
            request = requests.get(self.get_api_url())
            self.epg_data = json.loads(request.content)
            return self.epg_data

    def print_epg_data(self):
        print(json.dumps(self.get_epg_data(), indent=4))

    def get_program(self, title, offset=0):
        title = title.lower()
        epg_data = self.get_epg_data()
        airings = [elem for elem in epg_data if elem['program_title'].lower() == title]
        # print(json.dumps(airings, indent=4))
        if len(airings):
            return airings[offset]
        return None

    def contains(self, title):
        elem = self.get_program(title)
        if elem:
            return True
        else:
            return False

    def _from_iso_format(self, date_time):
        try:
            return datetime.datetime.fromisoformat(date_time)
        except AttributeError:
            utcoffset_pos = date_time.find('+')
            # Remove colons for backwards compatibility with %z directive
            while date_time[utcoffset_pos:].find(':') >= 0:
                colon_pos = utcoffset_pos + date_time[utcoffset_pos:].find(':')
                date_time = date_time[:colon_pos] + date_time[colon_pos + 1:]
            return datetime.datetime.strptime(date_time, '%Y-%m-%dT%H:%M:%S%z')

    def _from_utc_format(self, date_time):
        return datetime.datetime.utcfromtimestamp(date_time)\
            .replace(tzinfo=datetime.timezone.utc).astimezone(tz=None)

    def get_datetime(self, title, offset=0):
        try:
            elem = self.get_program(title, offset)
            if elem:
                return self._from_utc_format(elem['timestamp'])
        except IndexError:
            return None

    def get_start_time(self, title, offset=0):
        elem = self.get_program(title, offset)
        if elem:
            time = self._from_utc_format(elem['timestamp'])
            return time.strftime('%H:%M')


class PlayEPGv2(PlayEPGv1):
    """
    Class for fetching information from the Play API,
    as used in the Play program guide.
    Contains data for the channels Play4, Play5, Play6, Play7 and Play Crime.
    """
    channel = None
    date = None
    epg_data = None
    base_url = 'https://www.goplay.be/tv-gids/{channel}/{date}'

    def get_epg_data(self):
        if self.epg_data:
            return self.epg_data
        else:
            request = requests.get(self.get_api_url())
            self.epg_data = self._extract_epg_data(request.content)
            return self.epg_data

    def _extract_epg_data(self, page_content):
        # Extract the script tag that contains the epg data
        soup = BeautifulSoup(page_content, 'html.parser')
        # Find all script tags and traverse them in reverse order
        scripts = soup.find_all('script')
        for script in reversed(scripts):
            # Extract the script content
            script_str = script.string
            if script_str is not None:
                # Try to process the script content
                epg_data = self._process_script(script_str)
                if epg_data is not None:
                    return epg_data

    @classmethod
    def _last_dict_or_list_in(cls, iterable):
        if isinstance(iterable, dict):
            iterable = iterable.values()
        for item in reversed(iterable):
            if isinstance(item, dict):
                return item
            if isinstance(item, list):
                return item
        return None

    @classmethod
    def _determine_program_depth(cls, data, max_depth=20):
        depth = 0
        while depth < max_depth:
            if isinstance(data, dict) and 'program' in data:
                return depth
            else:
                data = cls._last_dict_or_list_in(data)
                if data is None:
                    return None
                else:
                    depth += 1

    @classmethod
    def _process_script(cls, script_str):
        """
        Try to extract the epg data from the script string.
        If unsuccessful, return None.
        """
        # Remove the next.js wrapping
        script_str = re.sub(r'self.__next_f.push\(\[\d+,"\w*:\w*', '', script_str)
        script_str = re.sub(r'"\]\)\Z', '', script_str)

        # Parse the escaped characters
        script_str = script_str.encode().decode('unicode_escape')

        # Try to parse the remaining string as json
        try:
            data_tree = json.loads(script_str)
        except json.JSONDecodeError:
            return None

        # Find the depth of the program data
        depth = cls._determine_program_depth(data_tree) - 2
        if depth is None:
            return None

        # Traverse the data tree to the correct depth
        for i in range(depth):
            data_tree = cls._last_dict_or_list_in(data_tree)

        # Extract the program data into a list
        epg_data = []
        for item in data_tree:
            iterable = cls._last_dict_or_list_in(item)
            if isinstance(iterable, dict) and 'program' in iterable:
                epg_data.append(iterable['program'])

        return epg_data

    def get_program(self, title, offset=0):
        title = title.lower()
        epg_data = self.get_epg_data()
        airings = [elem for elem in epg_data if elem['programTitle'].lower() == title]
        # print(json.dumps(airings, indent=4))
        if len(airings):
            return airings[offset]
        return None


class PlayEPG(PlayEPGv2):

    pass


class PlayDetailEPG:
    """
    Class for fetching episode information from the GoPlay website.
    """
    data = None

    def __init__(self, title, season, episode):
        self.title = title
        self.season = season
        self.episode = episode

    def get_url(self):
        url = 'https://www.goplay.be/video/{title}/{title}/{title}-s{season}-aflevering-{episode}'.format(
            title=self.title.lower().replace(' ', '-'),
            season=self.season,
            episode=self.episode
        )
        return url

    def get_data(self):
        if self.data:
            return self.data
        else:
            self.data = self._fetch_data(self.get_url())
            return self.data

    def _fetch_data(self, url):
        # Fetch the page content
        response = requests.get(url)
        response.raise_for_status()
        soup = BeautifulSoup(response.content, 'html.parser')
        # Extract the data from the head tag
        head = soup.head
        head_content = []
        for element in head.find_all():
            element_data = {
                'tag': element.name,
                'attributes': element.attrs,
                'text': element.get_text(strip=True),
            }
            head_content.append(element_data)
        return head_content

    def get_image_url(self):
        data = self.get_data()
        for element in data:
            if element['tag'] == 'meta' and 'property' in element['attributes']:
                if element['attributes']['property'] == 'og:image':
                    return element['attributes']['content']
        return None
