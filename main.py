#!/usr/bin/env python3
import argparse
import json
import os
import tweepy
from neighbours import *


if __name__ == '__main__':

    # Parse arguments
    parser = argparse.ArgumentParser(description='Tweet whether Neighbours (NL: Buren) is being broadcast.')
    parser.add_argument('-p', '--post', help='post to twitter', action='store_true')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-t', '--today', help='whether there is a broadcast', action='store_const', dest='type', const='today', default='today')
    group.add_argument('-s', '--summary', help='what the episode is about', action='store_const', dest='type', const='summary')
    group.add_argument('-m', '--missed', help='where to watch it later', action='store_const', dest='type', const='missed')
    args = parser.parse_args()

    # Load credentials
    with open('./twitter_auth.json') as file:
        credentials = json.load(file)

    # Authenticate v1.1 API to Twitter (for image upload)
    auth = tweepy.OAuthHandler(credentials['consumer_key'], credentials['consumer_secret'])
    auth.set_access_token(credentials['access_token'], credentials['access_token_secret'])
    api = tweepy.API(auth)
    api.verify_credentials()

    # Authenticate v2 client to Twitter
    client = tweepy.Client(
        consumer_key=credentials['consumer_key'], consumer_secret=credentials['consumer_secret'],
        access_token=credentials['access_token'], access_token_secret=credentials['access_token_secret']
    )

    # Prepare tweet
    if args.type == 'summary':
        tweet = NeighboursSummary()
    elif args.type == 'missed':
        tweet = NeighboursMissed()
    else:
        tweet = NeighboursToday()

    # Prepare content
    status = tweet.to_string()
    contains_image = tweet.contains_image()
    if contains_image:

        # Prepare image retrieval
        url = tweet.get_image_url()
        file_name = url[url.rfind('/') + 1:]
        if '?' in file_name:
            file_name = file_name[:file_name.index('?')]

        # Retrieve image
        with requests.get(url) as response:
            if response.status_code == 200:
                with open(file_name, 'wb') as image:
                    for chunk in response:
                        image.write(chunk)
            else:
                contains_image = False

    # Post tweet
    if contains_image:
        # Post status
        if args.post:
            media_list = []
            media = api.media_upload(file_name)
            media_list.append(media.media_id_string)
            client.create_tweet(text=status, media_ids=media_list)
            print('Status posted by istBuren:\n')
        else:
            print('Status drafted by istBuren:\n')
        print(status)
        print('Attached image:', url)

        # Remove temporary image file
        try:
            os.remove(file_name)
        except FileNotFoundError:
            pass

    elif tweet.contains_text():
        # Post status
        if args.post:
            client.create_tweet(text=status)
            print('Status posted by istBuren:\n')
        else:
            print('Status drafted by istBuren:\n')
        print(status)

    else:
        print('Status drafted by istBuren was empty.')
