import locale
import requests
from data import NeighboursData, NeighboursVrtData, NeighboursVtmData, NeighboursPlayData
from language_handler import Spice, SpiceNL, TextGeneratorNL


class NeighboursSpice(Spice):

    @classmethod
    def positive(cls):
        return cls.random_choice_from_hierarchy(
            ['😃','😊','😍','🥰','🤪','🤩','🥳','🙌','👏','🤘','🔥',['⭐','🌟','✨'],
             '😻','🍿','🎭','🥁','📺','🎁','🎈','🎉',['♥️','❤️','🧡','💛','💚','💙',
             '💜','💖','💝'],'🇦🇺']
        )

    @classmethod
    def negative(cls):
        return cls.random_choice_from_hierarchy(
            ['😒','😞','😔','😕','🙁','☹️','😖','😢','😭','🤬','😱','😨','😥','🤭','😧',
             '😿',['🤦‍♀️','🤦','🤦‍♂️'],['🤷‍♀️','🤷','🤷‍♂️'],['🙎‍♀️','🙎','🙎‍♂️'],'💔']
        )

    @classmethod
    def frustrated(cls):
        return cls.random_choice_from_hierarchy(
            ['😒','😕','☹️','😖','😢','😭','🤬','😱','😨','😥','🤭','😧',
             '😿',['🤦‍♀️','🤦','🤦‍♂️'],['🙎‍♀️','🙎','🙎‍♂️'],'💔']
        )


class NeighboursTweet(TextGeneratorNL):

    def __init__(self, channel='play5'):
        self.channel = channel
        if self.channel == 'een':
            self.data = NeighboursVrtData(0)
        elif self.channel == 'vtm2':
            self.data = NeighboursVtmData(0)
        elif self.channel == 'play5':
            self.data = NeighboursPlayData(0)
        else:
            raise NotImplementedError('NeighboursTweet supports \'play5\', \'vtm2\' and \'een\' as channels')

    def __str__(self):
        self._generate()
        return super().__str__()

    def to_string(self):
        return str(self)

    def contains_text(self):
        if self.to_string() != '' and self.to_string() != 'None':
            return True
        else:
            return False

    def contains_image(self):
        if self.get_image_url() != '' and self.get_image_url() != 'None' and self.get_image_url() is not None:
            return True
        else:
            return False

    def get_image_url(self):
        return None

    def _generate(self):
        raise NotImplementedError


class NeighboursToday(NeighboursTweet):

    def _generate(self):
        if self.channel == 'een':
            self.words = [self.spice.random_choice_from(['Het is vandaag', 'Vandaag is het']), '#Buren', 'op @een', '.']
        elif self.channel == 'vtm2':
            self.words = [self.spice.random_choice_from(['Het is vandaag', 'Vandaag is het']), '#Buren', 'op @VTM 2', '.']
        elif self.channel == 'play5':
            self.words = [self.spice.random_choice_from(['Het is vandaag', 'Vandaag is het']), '#Buren', 'op @Play5_be', '.']
        else:
            self.words = [self.spice.random_choice_from(['Het is vandaag', 'Vandaag is het']), '#Buren', '.']

        if not self.data:
            self._add_words([self.spice.unfortunately(0.75), self.spice.no()], 1)
            self._add_word(NeighboursSpice.negative())
        else:
            self._add_words(
                ['om', self.data.time],
                self.spice.random_choice_from([1, 2, 3])  # Insert at random position
            )
            self._add_word(NeighboursSpice.positive())
        self._add_word('\n#Neighbours')


class NeighboursSummary(NeighboursTweet):

    def _generate(self):
        self.words = []
        if self.data:
            self._add_word(self.spice.random_choice_from(['Straks', 'Deze namiddag', 'Vanmiddag']))
            self._add_word('in')
            self._add_word(self.spice.random_choice_from(['#RamsayStreet', '#Erinsborough']))
            self._add_word(':')
            self._add_word(self.data.subtitle_with_ellipsis)
            self._add_word('\n#Buren #Neighbours')

    def get_image_url(self):
        return self.data.image_url


class NeighboursMissed(NeighboursTweet):

    def _generate(self):
        try:
            locale.setlocale(locale.LC_ALL, 'nl_BE.utf8')
        except locale.Error:
            locale.setlocale(locale.LC_ALL, 'nl_BE')

        if self.data:
            self.words = ['#Buren', 'gemist', '?']
            self._add_words(
                ['op', self.data.datetime.strftime('%A'), self.data.datetime.strftime('%d %B').lstrip('0')],
                self.spice.random_choice_from([1, 2])  # Insert at random position
            )

            if self.channel == 'een':
                self._add_words(['Bekijk aflevering', self.data.episode, 'op @VRT_NU.'])
            elif self.channel == 'vtm2':
                self._add_words(['Bekijk aflevering', self.data.episode, 'op #VTMGO.'])
            elif self.channel == 'play5':
                self._add_words(['Bekijk aflevering', self.data.episode, 'op @GoPlay__be.'])
            else:
                self.words = []
                return

            self._add_word('\n#Neighbours')
            self._add_word('\n' + self.data.on_demand_url)
        else:
            self.words = []
