# istBuren

Assistant that tells when the Australian soap opera Neighbours (NL: Buren) is being broadcast on the Flemish television.
Offers three types of tweets: `today`, `summary` and `missed`.
See [some examples](#examples), or [instructions to get started](#installation).

```shell
usage: main.py [-h] [-p] [-t | -s | -m]

optional arguments:
  -h, --help     show this help message and exit
  -p, --post     post to twitter
  -t, --today    whether there is a broadcast
  -s, --summary  what the episode is about
  -m, --missed   where to watch it later
```

## Examples

### Today

>  Vandaag is het [\#Buren](https://twitter.com/hashtag/Buren) op [\@een](https://twitter.com/een) om 16:16. 🥁
>  
>  [\#Neighbours](https://twitter.com/hashtag/Neighbours)


### Summary

>  Vanmiddag in [\#Erinsborough](https://twitter.com/hashtag/Erinsborough): Aaron en David nemen een belangrijke beslissing…
>  
>  [\#Buren](https://twitter.com/hashtag/Buren) [\#Neighbours](https://twitter.com/hashtag/Neighbours)
>  
>  <img src="https://images.vrt.be/orig/2020/04/08/9f4ce768-79b5-11ea-aae0-02b7b76bf47f.jpg" alt="attached image" width="400"/>


### Missed

>  [\#Buren](https://twitter.com/hashtag/Buren) gemist op vrijdag 10 april? Bekijk aflevering 7982 op [\@VRT_NU](https://twitter.com/VRT_NU).
>  
>  [\#Neighbours](https://twitter.com/hashtag/Neighbours)
>  
>  https://www.vrt.be/vrtnu/a-z/buren/2020/buren-s2020a7982/


## Installation

### Clone the project
```shell
git clone https://gitlab.com/BCLegon/istburen.git
cd istburen
```

### Install the dependencies
```shell
pip3 install --user pipenv
python3 -m pipenv install
```

### Add your credentials
Update the fields in `twitter_auth.json` to the codes you generated [here](https://developer.twitter.com/en/apps). Then run the following command.
```shell
git update-index --skip-worktree twitter_auth.json
```
This [prevents git](https://stackoverflow.com/a/13631525/717372) from accidentally tracking your credentials.

### Test run the script
```shell
python3 -m pipenv run python main.py
```
This logs into the twitter API using your credentials and drafts the status to tweet, but doesn't post it.

To post the tweet, use the `-p` or `--post` flag when running the script.
You should see a tweet appear on the account you connected through your credentials.

### Add a cron job
```shell
crontab -e
0 10 * * * cd $HOME/istburen && python3 -m pipenv run python main.py -p
```
This runs the script every day at 10 am.

If you want to add cron jobs for all types of tweets, this can be done in the following way.
```shell
crontab -e
0 10 * * * cd $HOME/istburen && python3 -m pipenv run python main.py --today -p
0 13 * * * cd $HOME/istburen && python3 -m pipenv run python main.py --summary -p
0 19 * * * cd $HOME/istburen && python3 -m pipenv run python main.py --missed -p
```
